from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Ahmad Dzikrul Fikri' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,7,29) #TODO Implement this, format (Year, Month, Date)
npm = 1806196806 # TODO Implement this
angkatan = 2018
universitas = 'Universitas Indonesia'
hobi = 'Tidur sambil main HP'
deskripsi = 'Saya adalah pribadi yang senang mencoba hal baru. Saya berharap mendapatkan pengalaman yang nantinya akan berguna bagi kehidupan saya'
SD = "SDN Bojongrangkas 04"
SMP = "SMPN 1 Cibungbulang"
SMA = "SMAN 1 Leuwiliang"
sosmed1 = "ahmaddzikrulfikri"
sosmed2 = "Ahmad Dzikrul Fikri"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'angkatan': angkatan, 'universitas':universitas,'hobi':hobi, 'deskripsi': deskripsi, 'sosmed1':sosmed1, 'sosmed2':sosmed2,'SD':SD, 'SMP':SMP,'SMA':SMA}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
